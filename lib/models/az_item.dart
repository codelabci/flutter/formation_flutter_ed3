import 'package:azlistview/azlistview.dart';

class AzItem extends ISuspensionBean {
  String title;
  String tag;
  AzItem({
    required this.title,
    required this.tag,
  });

  // @override
  // String getSuspensionTag() {
  //   // TODO: implement getSuspensionTag
  //   throw UnimplementedError();
  // }

  @override
  String getSuspensionTag() => tag;
}
