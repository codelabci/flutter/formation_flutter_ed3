import 'dart:convert';

class User {
  // late String nom; // assurance d'initialisation apres
  // String? nom1; // la variable peut etre null
  int? id;
  String nom;
  String? email;
  String? prenoms;
  String? password;
  User({
    this.id,
    required this.nom,
    this.email,
    this.prenoms,
    this.password,
  });
  // bool isActive;

  User copyWith({
    int? id,
    String? nom,
    String? email,
    String? prenoms,
    String? password,
  }) {
    return User(
      id: id ?? this.id,
      nom: nom ?? this.nom,
      email: email ?? this.email,
      prenoms: prenoms ?? this.prenoms,
      password: password ?? this.password,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'nom': nom,
      'email': email,
      'prenoms': prenoms,
      'password': password,
    };
  }

  factory User.fromMap(Map<String, dynamic> map) {
    return User(
      id: map['id']?.toInt(),
      nom: map['nom'] ?? '',
      email: map['email'],
      prenoms: map['prenoms'],
      password: map['password'],
    );
  }

  String toJson() => json.encode(toMap());

  factory User.fromJson(String source) => User.fromMap(json.decode(source));

  @override
  String toString() {
    return 'User(id: $id, nom: $nom, email: $email, prenoms: $prenoms, password: $password)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is User &&
        other.id == id &&
        other.nom == nom &&
        other.email == email &&
        other.prenoms == prenoms;
  }

  @override
  int get hashCode {
    return id.hashCode ^ nom.hashCode ^ email.hashCode ^ prenoms.hashCode;
  }
}
