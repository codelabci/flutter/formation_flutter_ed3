import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ThemeChanger with ChangeNotifier {
  ThemeData _themeData;

  ThemeChanger(this._themeData);
  getTheme() => _themeData;
  setTheme(ThemeData themeData) {
    _themeData = themeData;
    notifyListeners(); // informer les suscribers
  }

  initTheme() {
    _themeData = ThemeData.light();
    notifyListeners(); // informer les suscribers
  }
}
