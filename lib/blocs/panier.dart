import 'package:flutter/material.dart';

class PanierChanger with ChangeNotifier {
  int quantite;
  String name;

  PanierChanger(this.name, this.quantite);

  setPanier(String newName, {int newQuantite = 1}) {
    if (newName != name) {
      name = newName;
      quantite = newQuantite;
    } else {
      quantite += newQuantite;
    }
    notifyListeners();
  }

  get getName {
    return name;
  }

  get getQuantite {
    return quantite;
  }

  get getPanier {
    return "$name : $quantite";
  }
}
