import 'package:flutter/material.dart';

class LangageChanger with ChangeNotifier {
  Locale _locale;

  LangageChanger(this._locale);
  getLocale() => _locale;
  setLocale(Locale locale) {
    _locale = locale;
    notifyListeners();
  }

  initLangage() {
    _locale = const Locale('fr');
    notifyListeners();
  }
}
