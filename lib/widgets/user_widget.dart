import 'package:flutter/material.dart';
import 'package:formation_flutter_ed3/helpers/constant.dart';
import 'package:formation_flutter_ed3/helpers/repository.dart';
import 'package:formation_flutter_ed3/helpers/utilities.dart';
import 'package:formation_flutter_ed3/models/user.dart';
import 'package:formation_flutter_ed3/pages/edit_user.dart';
import 'package:formation_flutter_ed3/pages/home.dart';

class UserWidget extends StatelessWidget {
  User user;
  bool isUseInDatabase;
  UserWidget({
    Key? key,
    required this.user,
    this.isUseInDatabase = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(user.nom),
      subtitle: Text(user.email ?? ""),
      leading: const CircleAvatar(
        child: Icon(Icons.settings),
      ),
      trailing: IconButton(
          onPressed: () {
            print("Voulez vous m'editer ?");

            Utilites.navigatorPush(
                context: context, view: EditUserPage(user: user));
          },
          icon: const Icon(Icons.edit)),
      onTap: () {
        // logique de suppression
        showDialog(
            // barrierLabel: "Merci",
            context: context,
            builder: (context) {
              return AlertDialog(
                // elevation: 0,
                // alignment: Alignment.center,
                // actionsPadding: EdgeInsets.all(20),
                actionsAlignment: MainAxisAlignment.spaceBetween,
                title: Text("Suppression"),
                content:
                    Text("etes vous sur de vouloir supprimer cet element ?"),
                actions: [
                  TextButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text("Non"),
                  ),
                  TextButton(
                    onPressed: () {
                      print("begin ${datasUser.length}");

                      Navigator.pop(context);
                      isUseInDatabase
                          ? SQLHelper.deleteUser(user.id!)
                          : datasUser.remove(user);
                      print("after ${datasUser.length}");
                      Utilites.navigatorPushAndRemoveUntil(
                          context: context, view: HomePage());
                    },
                    child: Text("Oui"),
                  ),
                ],
              );
            });

        print("Voulez vous me supprimez ?");
      },
    );
  }
}
