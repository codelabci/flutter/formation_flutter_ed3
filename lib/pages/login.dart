import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:formation_flutter_ed3/helpers/constant.dart';
import 'package:formation_flutter_ed3/helpers/preferences_util.dart';
import 'package:formation_flutter_ed3/helpers/styles.dart';
import 'package:formation_flutter_ed3/helpers/utilities.dart';
import 'package:formation_flutter_ed3/models/user.dart';
import 'package:formation_flutter_ed3/pages/home.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  late SharedPreferences prefs;
  String email = "email";
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initPreferences();
    // PreferencesUtil.instance.putString(email, "coulctm");
  }

  initPreferences() async {
    print("initPreferences");
    prefs = await SharedPreferences.getInstance();
    await prefs.setString(email, "coulctm");
  }

  TextEditingController _textEditingControllerPassword =
      new TextEditingController();
  TextEditingController _textEditingControllerEmail =
      new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue,
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.2,
                child: Image.asset(
                  "images/logo.jpg",
                  fit: BoxFit.contain,
                ),
              ),
              Utilites.getHeightSizedBox(context),

              // Image.asset(
              //   "images/logo.jpg",
              // ),
              Text(
                structureName.toUpperCase(),
                style: stAppName,
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.80,
                child: Card(
                  shape: Utilites.getDefaultShape(raduis: 20),
                  // decoration: BoxDecoration(color: Colors.white),
                  child: Form(
                    child: Padding(
                      padding: const EdgeInsets.all(30),
                      child: Column(
                        children: [
                          TextFormField(
                            controller: _textEditingControllerEmail,
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.zero,
                              hintText: "Email",
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(40),
                                borderSide: BorderSide.none,
                              ),
                              filled: true,
                              fillColor: Colors.grey.shade200,
                              prefixIcon: const Icon(
                                Icons.email,
                                // color: Colors.grey,
                              ),
                            ),
                          ),
                          Utilites.getHeightSizedBox(context),
                          TextFormField(
                            controller: _textEditingControllerPassword,
                            decoration: InputDecoration(
                                contentPadding: EdgeInsets.zero,
                                hintText: "Password",
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(40),
                                  borderSide: BorderSide.none,
                                ),
                                filled: true,
                                fillColor: Colors.grey.shade200,
                                prefixIcon: const Icon(
                                  Icons.lock,
                                  // color: Colors.grey.shade100,
                                ),
                                suffixIcon: const Icon(
                                  Icons.remove_red_eye,
                                  // color: Colors.grey.shade100,
                                )),
                          ),
                          Utilites.getHeightSizedBox(context),

                          InkWell(
                            onTap: () {},
                            child: const Text(
                              "Forgot password ?",
                              style: TextStyle(color: Colors.grey),
                            ),
                            // style: ButtonStyle(),
                          ),

                          ElevatedButton(
                            onPressed: () async {
                              // print(" prefs :: $prefs");

                              // String lastEmail = await PreferencesUtil.instance
                              //     .getString(email);

                              String? lastEmail = prefs.getString(
                                email,
                              );
                              print("lastEmail :: $lastEmail");

                              if (lastEmail != null &&
                                  lastEmail ==
                                      _textEditingControllerEmail.text
                                          .toString()) {
                                Utilites.navigatorPush(
                                    context: context, view: HomePage());
                              } else {
                                print("error login");
                              }

                              //

                              String password = _textEditingControllerPassword
                                  .text
                                  .toString();

                              String emailSaisie =
                                  _textEditingControllerEmail.text.toString();

                              // appel de service

                              var urlPost = Uri.http(
                                  '192.168.1.107:8080', 'user/connexion');

                              User user = User(nom: "");

                              Map<String, String> headers = {
                                "Content-type": "application/json"
                              };

                              user.email = emailSaisie;
                              user.password = password;
                              var response = await http.post(urlPost,
                                  body: convert.json.encode({
                                    "data": {
                                      "email": emailSaisie,
                                      "password": password
                                    }
                                  }),
                                  headers: headers);

                              if (response.statusCode == 200) {
                                var body = convert.jsonDecode(response.body);

                                print('body: $body.');
                              } else {
                                print(
                                    'Request failed with status: ${response.statusCode}.');
                              }

                              // prefs.setString("email", "");
                            },
                            style: ButtonStyle(
                              shape: Utilites.getShapeElevatedButton(),
                            ),
                            child: const Text(
                              "Login",
                            ),
                            // style: ButtonStyle(),
                          ),
                          InkWell(
                            onTap: () {},
                            child: const Text(
                              "SIGNUP",
                              style: TextStyle(color: Colors.grey),
                            ),
                            // style: ButtonStyle(),
                          ),
                          // InkWell(

                          //   onTap: () {},
                          //   child: Text(
                          //     "Forgot password ?",
                          //     style: TextStyle(color: Colors.grey),
                          //   ),
                          // )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
