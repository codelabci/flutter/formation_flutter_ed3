import 'dart:io';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sound/flutter_sound.dart';
import 'package:permission_handler/permission_handler.dart';

class MicroPage extends StatefulWidget {
  const MicroPage({super.key});

  @override
  State<MicroPage> createState() => _MicroPageState();
}

class _MicroPageState extends State<MicroPage> {
  final audioPlayer = AudioPlayer(); // pour la lecture de l'enregistrement

  String urlFile = "";
  Duration duration = Duration.zero;
  Duration position = Duration.zero;
  bool isRecorderReady = false;
  Future<dynamic> record() async {
    if (!isRecorderReady) return;
    await recorder.startRecorder(toFile: "audio");
  }

  Future<dynamic> stop() async {
    if (!isRecorderReady) return;
    final path = await recorder.stopRecorder();

    final audioFile = File(path!);

    urlFile = path;

    print("Path enregistrement : $path ");
    print("Fichier enregistrer : $audioFile ");
  }

  bool isPlay = false;

  Future<dynamic> initRecorder() async {
    final status = await Permission.microphone.request();

    if (status != PermissionStatus.granted) {
      throw "Micro phone not granted";
    }
    await recorder.openRecorder(); // on doit l'ouvrir avant usage

    isRecorderReady = true;
    recorder.setSubscriptionDuration(
      const Duration(milliseconds: 500),
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    initRecorder();

    initAudio();

// listen to state : playing, paused, stopper
    audioPlayer.onPlayerStateChanged.listen((event) {
      print("audioPlayer.onDurationChanged");

      setState(() {
        isPlay = event.toString() == "playing";
      });
    });

    // listen to audio duration
    audioPlayer.onDurationChanged.listen((newDuration) {
      print("audioPlayer.onDurationChanged");
      setState(() {
        duration = newDuration;
      });
    });

    // listen to audio duration
    audioPlayer.onPositionChanged.listen((newPosition) {
      print("audioPlayer.onPositionChanged");

      setState(() {
        position = newPosition;
      });
    });
  }

  Future initAudio() async {
    audioPlayer.setReleaseMode(ReleaseMode.loop); // lecture en boucle

    // charger l'audio
    String urlAudio = "https://server8.mp3quran.net/ayyub/001.mp3";

    if (urlFile.isEmpty) {
      audioPlayer.setSourceUrl(urlAudio);
    } else {
      audioPlayer.setSourceDeviceFile(urlFile);
    }
    // audioPlayer.setSourceUrl(url);
  }

  final recorder = FlutterSoundRecorder();

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();

    recorder.closeRecorder();
    audioPlayer.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            StreamBuilder<RecordingDisposition>(
              stream: recorder.onProgress,
              builder: ((context, snapshot) {
                final duration =
                    snapshot.hasData ? snapshot.data!.duration : Duration.zero;

                return Text(
                  "${duration.inSeconds} seconds",
                  style: TextStyle(fontSize: 80, fontWeight: FontWeight.bold),
                );
              }),
            ),
            IconButton(
              onPressed: () async {
                if (recorder.isRecording) {
                  await stop();
                } else {
                  await record();
                }

                setState(() {});
              },
              iconSize: 80,
              icon: Icon(
                recorder.isRecording ? Icons.stop : Icons.mic_rounded,
              ),
            ),
            Spacer(),
            Container(
              color: Colors.grey.shade200,
              alignment: Alignment.bottomCenter,
              child: Column(
                children: [
                  Slider(
                      min: 0,
                      max: duration.inSeconds.toDouble(),
                      value: position.inSeconds.toDouble(),
                      onChanged: (value) async {
                        final position = Duration(seconds: value.toInt());
                        await audioPlayer.seek(position);

                        audioPlayer.resume(); // optional
                      }),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 30.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(formatTime(position)),
                        Text(formatTime(duration - position)),
                      ],
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      IconButton(
                        onPressed: () async {
                          if (isPlay) {
                            await audioPlayer.pause();
                          } else {
                            await audioPlayer.resume();
                          }

                          setState(() {
                            isPlay = !isPlay;
                          });
                        },
                        iconSize: 50,
                        icon: Icon(
                          isPlay ? Icons.pause_circle : Icons.play_circle,
                        ),
                      ),
                      IconButton(
                        onPressed: () async {
                          await audioPlayer.dispose();
                          await initAudio();
                          setState(() {});
                        },
                        iconSize: 50,
                        icon: Icon(
                          Icons.refresh,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  formatTime(Duration duration) {
    int nbreMin = duration.inMinutes;
    int nbreSeconds = duration.inSeconds;
    return "$nbreMin : $nbreSeconds";
  }
}
