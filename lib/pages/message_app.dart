import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:formation_flutter_ed3/helpers/constant.dart';
import 'package:formation_flutter_ed3/helpers/utilities.dart';
import 'package:formation_flutter_ed3/pages/chat.dart';

class MessageApp extends StatefulWidget {
  MessageApp({Key? key}) : super(key: key);

  @override
  State<MessageApp> createState() => _MessageAppState();
}

class _MessageAppState extends State<MessageApp> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // backgroundColor: Colors.transparent,
        elevation: 0,
        title: Text("Messages"),
        centerTitle: true,
        automaticallyImplyLeading:
            false, // annule tout ce qui est a droit du title si le leading est null
        // leading: Icon(Icons.abc),
        actions: [
          PopupMenuButton(itemBuilder: (context) {
            return datasPopupMenuButtonMessage.map((String choice) {
              return PopupMenuItem<String>(
                value: choice,
                child: Text(choice),
                onTap: () {},
              );
            }).toList();
          })
        ],
      ),

      // drawer:
      //     Drawer(), // l'icone du drawer s'affiche s'il leading == null ou automaticallyImplyLeading: false

      body: ListView.builder(
        itemBuilder: (context, index) {
          var data = datasAllFileMessage[index];
          // Dismissible(key: key, child: child)

          return Slidable(
            child: ItemMessage(data: data),
            startActionPane:
                ActionPane(children: [], motion: Icon(Icons.share)),
            endActionPane: ActionPane(children: [], motion: Icon(Icons.delete)),
          );
        },
        itemCount: datasAllFileMessage.length,
      ),
    );
  }
}

class ItemMessage extends StatelessWidget {
  const ItemMessage({
    Key? key,
    required this.data,
  }) : super(key: key);

  final Map<String, Object> data;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      isThreeLine: true,
      selected: data["isShow"] as bool,
      title: Text(data["name"] as String),
      subtitle: Text(
        data["lastContent"] as String,
        maxLines: 3,
        overflow: TextOverflow.ellipsis,
      ),
      leading: CircleAvatar(
        backgroundColor: (data["color"] as Color).withOpacity(0.7),
        child: const Icon(Icons.person_rounded),
      ),
      trailing: data["isShow"] as bool
          ? Wrap(
              crossAxisAlignment: WrapCrossAlignment.center,
              children: [
                Text(data["heure"] as String),
                const Icon(Icons.noise_control_off)
              ],
            )
          : Text(data["heure"] as String),
      onTap: () {
        Utilites.navigatorPush(
          context: context,
          view: ChatPage(currentMessage: data),
        );
      },
    );
  }
}
