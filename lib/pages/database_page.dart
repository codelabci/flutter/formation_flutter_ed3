import 'package:flutter/material.dart';
import 'package:formation_flutter_ed3/pages/edit_user.dart';
import 'package:formation_flutter_ed3/pages/home.dart';

class DatabasePage extends StatefulWidget {
  DatabasePage({Key? key}) : super(key: key);

  @override
  State<DatabasePage> createState() => _DatabasePageState();
}

class _DatabasePageState extends State<DatabasePage> {
  @override
  Widget build(BuildContext context) {
    print("object");
    return DefaultTabController(
      length: 2,
      initialIndex: 1,
      child: Scaffold(
        appBar: AppBar(
          title: Text("Use Database"),
          bottom: TabBar(tabs: [
            Tab(
              text: "All users",
            ),
            Tab(
              text: "Edit users",
            )
          ]),
        ),
        body: TabBarView(
          children: [
            HomePage(isUseInDatabase: true),
            EditUserPage(isUseInDatabase: true),
          ],
        ),
      ),
    );
  }
}
