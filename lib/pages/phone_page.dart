import 'package:azlistview/azlistview.dart';
import 'package:flutter/material.dart';
import 'package:formation_flutter_ed3/helpers/constant.dart';
import 'package:formation_flutter_ed3/models/az_item.dart';
import 'package:grouped_list/grouped_list.dart';

class PhoneAppPage extends StatefulWidget {
  PhoneAppPage({Key? key}) : super(key: key);

  @override
  State<PhoneAppPage> createState() => _PhoneAppPageState();
}

class _PhoneAppPageState extends State<PhoneAppPage> {
  List<AzItem> datasAzItem = [];

  bool isSearching = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initListAzListView();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      initialIndex: 1,
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false, // retirer la fleche de retour
          title: isSearching
              ? TextFormField(
                  // cursorColor: Colors.white,
                  decoration: InputDecoration(
                    suffix: IconButton(
                      icon: Icon(Icons.visibility),
                      onPressed: () {
                        isSearching = false;
                        setState(() {});
                      },
                    ),
                  ),
                )
              : const Text("Téléphone"),
          bottom: const TabBar(
            tabs: [
              Tab(
                child: Icon(Icons.cloud),
              ),
              Tab(
                child: Icon(Icons.beach_access),
              ),
              Tab(
                child: Icon(Icons.brightness_5),
              ),
            ],
          ),
          actions: isSearching
              ? null
              : [
                  IconButton(
                      onPressed: () {
                        isSearching = true;
                        setState(() {});
                      },
                      icon: Icon(Icons.search)),
                  PopupMenuButton(
                    itemBuilder: ((context) {
                      return datasPopupMenuButton.map((String choice) {
                        return PopupMenuItem<String>(
                          value: choice,
                          child: Text(choice),
                          onTap: () {
                            print("on a clicker sur moi :: $choice");
                          },
                        );
                      }).toList();
                    }),
                  ),
                ],
        ),
        body: TabBarView(
          // controller: tabController,
          children: <Widget>[
            const FirstContentWidget(),
            const SecondContentWidget(),
            ThreeContentWidget(datasAzItem: datasAzItem)
          ],
        ),
      ),
    );
  }

  initListAzListView() {
    datasAzItem = datasContact
        .map((e) => AzItem(title: e, tag: e[0].toUpperCase()))
        .toList();

    SuspensionUtil.sortListBySuspensionTag(datasAzItem); // ordonner la list/tag

    setState(() {});
  }
}

class ThreeContentWidget extends StatelessWidget {
  const ThreeContentWidget({
    Key? key,
    required this.datasAzItem,
  }) : super(key: key);

  final List<AzItem> datasAzItem;

  @override
  Widget build(BuildContext context) {
    return AzListView(
      data: datasAzItem,
      itemCount: datasAzItem.length,
      itemBuilder: (context, index) {
        var data = datasAzItem[index];
        return ListTile(
          onTap: () {},
          title: Text(data.title),
          leading: CircleAvatar(
              backgroundColor: getColor(context, data.title[0]),
              child: Text(data.title[0],
                  style: const TextStyle(
                    fontWeight: FontWeight.w300,
                    fontSize: 30,
                  ))),
        );
      },
      indexHintBuilder: (context, hint) => Container(
          alignment: Alignment.center,
          height: 60,
          width: 60,
          decoration: BoxDecoration(
              shape: BoxShape.circle, color: Theme.of(context).primaryColor),
          child: Text(hint,
              style: const TextStyle(
                color: Colors.red,
                fontSize: 30,
              ))),
      indexBarOptions: IndexBarOptions(
        needRebuild: true, // maj la liste lors du scroll sur l'alphabet
        indexHintAlignment: Alignment.centerRight,
        selectTextStyle:
            const TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
        selectItemDecoration: BoxDecoration(
            shape: BoxShape.circle, color: Theme.of(context).primaryColor),
      ),
    );
  }
}

class SecondContentWidget extends StatelessWidget {
  const SecondContentWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GroupedListView<dynamic, String>(
      elements: datasCurrentCall,
      groupBy: (element) => element['group'],
      groupHeaderBuilder: (element) {
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            element["group"],
            style: const TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
        );
      },
      itemBuilder: (context, element) {
        return ListTile(
          contentPadding:
              const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
          leading: CircleAvatar(
            backgroundColor: getColor(context, element["name"][0]),
            child: Text(
              element["name"][0],
              style: const TextStyle(fontWeight: FontWeight.w300, fontSize: 20),
            ),
          ),
          title: Text(
            element["name"],
            style: const TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
          subtitle: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                element["phone"],
              ),
              Row(
                children: [
                  Stack(
                    alignment: AlignmentDirectional.center,
                    children: [
                      Container(
                        // height: 50,
                        padding: const EdgeInsets.all(2),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: element["sim"] == 1
                                ? Colors.green
                                : Colors.blue),
                        child: const Icon(
                          Icons.sim_card,
                          size: valSizeIcon,
                          color: Colors.black,
                        ),
                      ),
                      Text(
                        '${element["sim"]}',
                        style:
                            const TextStyle(color: Colors.white, fontSize: 10),
                      ),
                    ],
                  ),
                  const SizedBox(width: valWidthSizedbox),
                  Icon(
                    element["sim"] == 1 ? Icons.north_east : Icons.south_east,
                    size: valSizeIcon,
                  ),
                  const SizedBox(width: valWidthSizedbox),
                  Text(
                    '${element["heure"]}, ',
                  ),
                  Text(
                    '${element["type"]}',
                  ),
                ],
              )
            ],
          ),
          trailing: IconButton(
            onPressed: () {},
            icon: const Icon(Icons.phone),
          ),
        );
      },
    );
  }
}

getColor(BuildContext context, String value) {
  return value == 'A'
      ? Theme.of(context).primaryColorLight.withOpacity(0.5)
      : value == 'K'
          ? Colors.orange.withOpacity(0.5)
          : Theme.of(context).errorColor.withOpacity(0.5);
}

class ContactWidget extends StatelessWidget {
  String name;

  double height;
  double width;
  double size;
  ContactWidget(
      {Key? key,
      required this.name,
      this.height = 100,
      this.size = 40,
      this.width = 100})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      // height: 200,
      width: 140,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Stack(
          alignment: AlignmentDirectional.topEnd,
          children: [
            IconButton(
                onPressed: () {
                  showModalBottomSheet<void>(
                    // shape: Utilities.getDefaultShape(),
                    backgroundColor: Colors.transparent,
                    context: context,
                    builder: (BuildContext context) {
                      return Container(
                        // height: 200,
                        decoration: BoxDecoration(
                          color: Colors.blue.shade50,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(valRaduis),
                            topRight: Radius.circular(valRaduis),
                          ),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            const Text('Modal BottomSheet'),
                            ElevatedButton(
                              child: const Text('Close BottomSheet'),
                              onPressed: () => Navigator.pop(context),
                            ),
                          ],
                        ),
                      );
                    },
                  );
                },
                icon: const Icon(Icons.more_vert)),
            Center(
              child: SizedBox(
                  height: height,
                  width: width,
                  child: CircleAvatar(
                    backgroundColor: name[0].contains("A")
                        ? Theme.of(context).primaryColor.withOpacity(0.5)
                        : name[0].contains("M")
                            ? Theme.of(context).errorColor.withOpacity(0.5)
                            : Theme.of(context)
                                .primaryColorLight
                                .withOpacity(0.5),
                    child: Text(
                      name[0],
                      style: TextStyle(
                          fontWeight: FontWeight.w200, fontSize: size),
                    ),
                  )),
            ),
            // Center(
            //   child: SizedBox(
            //     height: height,
            //     width: width,
            //     child: Container(
            //       alignment: Alignment.center,
            //       decoration: BoxDecoration(
            //         borderRadius: BorderRadius.circular(300),
            //         color: name[0].contains("A")
            //             ? Theme.of(context).primaryColor.withOpacity(0.5)
            //             : name[0].contains("M")
            //                 ? Theme.of(context).errorColor.withOpacity(0.5)
            //                 : Theme.of(context)
            //                     .primaryColorLight
            //                     .withOpacity(0.5),
            //       ),
            //       child: Text(
            //         name[0],
            //         style:
            //             TextStyle(fontWeight: FontWeight.w200, fontSize: size),
            //       ),
            //     ),
            //   ),
            // ),

            Align(
              alignment: Alignment.bottomCenter,
              child: Text(
                name,
                style: const TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class FirstContentWidget extends StatelessWidget {
  const FirstContentWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      // padding: const EdgeInsets.all(8),
      children: [
        Container(
          color: Colors.grey.shade200,
          child: const Padding(
            padding: EdgeInsets.only(left: 16.0, right: 16, top: 16),
            child: Text(
              "Appels suggéres",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
        ),
        Container(
          color: Colors.red.shade200,
          child: SizedBox(
            height: MediaQuery.of(context).size.height *
                0.18, // pour avoir une hauteur responsive
            child: ListView.builder(
              itemBuilder: (context, index) {
                return ContactWidget(
                    name: datasContact[index], height: 60, width: 60, size: 30);
              },
              itemCount: datasContact.length,
              scrollDirection: Axis.horizontal,
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: const [
              Text(
                "Favoris",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              // Icon(Icons.zoom_out_map)
              Icon(Icons.zoom_in_map)
            ],
          ),
        ),
        GridView(
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            crossAxisSpacing: 10,
            mainAxisSpacing: 10,
          ),
          shrinkWrap: true, // You won't see infinite size error
          physics:
              const NeverScrollableScrollPhysics(), // pour empecher le scroll
          children: [
            for (var name in datasContact)
              Container(
                child: ContactWidget(name: name),
                color: Colors.blue.shade100,
              ),
          ],
        )
      ],
    );
  }
}
