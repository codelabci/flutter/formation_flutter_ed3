import 'package:flutter/material.dart';
import 'package:formation_flutter_ed3/helpers/constant.dart';
import 'package:grouped_list/grouped_list.dart';

class ChatPage extends StatefulWidget {
  ChatPage({Key? key, required this.currentMessage}) : super(key: key);
  dynamic currentMessage;

  @override
  State<ChatPage> createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  final TextEditingController _controller = TextEditingController();
  String valueSender = "";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.currentMessage["name"]),
      ),
      body: Column(
        children: [
          Expanded(
            child: GroupedListView<dynamic, String>(
                useStickyGroupSeparators:
                    true, // laisse le titre du group flottant
                floatingHeader: true, // retire le fond du header

                // reverse: true,

                elements: datasContentFileMessage,
                groupSeparatorBuilder: (value) {
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        padding: const EdgeInsets.all(8.0),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(valRaduis),
                          color: Colors.grey.shade200,
                        ),
                        child: Text(
                          value,
                          textAlign: TextAlign.center,
                          style: const TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ],
                  );
                },
                // groupComparator: (value1, value2) => value1.compareTo(value2),
                groupBy: (element) => element['jour'],
                // groupHeaderBuilder: (element) {
                //   return Padding(
                //     padding: const EdgeInsets.all(8.0),
                //     child: Center(
                //       child: Container(
                //         padding: const EdgeInsets.all(8.0),
                //         decoration: BoxDecoration(
                //           borderRadius: BorderRadius.circular(valRaduis),
                //           color: Colors.grey.shade200,
                //         ),
                //         child: Text(element["jour"],
                //             style: const TextStyle(
                //               fontWeight: FontWeight.bold,
                //             )),
                //       ),
                //     ),
                //   );
                // },
                itemBuilder: (context, element) {
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: element['isMe']
                          ? MainAxisAlignment.end
                          : MainAxisAlignment.start,
                      children: [
                        if (!element['isMe']) const CircleAvatar(),
                        const SizedBox(
                          width: 5,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width * 0.7,
                          // margin: const EdgeInsets.all(8.0),
                          padding: const EdgeInsets.all(8.0),
                          decoration: BoxDecoration(
                              color: element['isMe']
                                  ? Colors.grey.shade300
                                  : Colors.grey.shade200,
                              borderRadius: BorderRadius.only(
                                bottomLeft:
                                    element['isMe'] ? raduis : Radius.zero,
                                bottomRight:
                                    element['isMe'] ? Radius.zero : raduis,
                                topLeft: raduis,
                                topRight: raduis,
                              )),
                          child: Text(element['content']),
                        ),
                      ],
                    ),
                  );
                }),
          ),
          Container(
            // color: Colors.grey.shade200,
            alignment: Alignment.bottomCenter,
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                Expanded(
                  child: TextField(
                    controller: _controller,
                    onChanged: (value) {
                      print(value);

                      valueSender = value;
                      setState(() {});
                    },
                    // maxLines: 3,
                    decoration: InputDecoration(
                      fillColor: Colors.grey.shade200,
                      filled: true,
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(50)),
                    ),
                    keyboardType: TextInputType.multiline,
                  ),
                ),
                IconButton(
                  onPressed:
                      _controller.text != null && _controller.text.isNotEmpty
                          ? () {
                              datasContentFileMessage.add(
                                {
                                  "content": _controller.text,
                                  "isMe": true,
                                  "jour": "Hier",
                                },
                              );
                              datasContentFileMessage.add(
                                {
                                  "content": "j'ai recu ${_controller.text}",
                                  "isMe": false,
                                  // "jour": "Aujourd'hui",
                                  "jour": "Hier",
                                },
                              );

                              _controller.text = "";
                              setState(() {});
                            }
                          : null,
                  icon: const Icon(
                    Icons.send,
                    size: 40,
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
