import 'package:flutter/material.dart';
import 'package:formation_flutter_ed3/helpers/constant.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class ApiPage extends StatefulWidget {
  ApiPage({Key? key}) : super(key: key);

  @override
  State<ApiPage> createState() => _ApiPageState();
}

class _ApiPageState extends State<ApiPage> {
  TextEditingController _controller = TextEditingController();
  dynamic datas = [];

  bool searching = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Recherche")),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
              Expanded(
                child: TextField(
                  controller: _controller,
                  decoration: InputDecoration(
                    labelText: "Nom",
                    hintText: "Saisir nom ...",
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(valBorderRaduis)),
                  ),
                ),
              ),
              IconButton(
                  onPressed: () => getDatas(value: _controller.text.toString()),
                  icon: const Icon(Icons.search))
            ]),
            const SizedBox(height: 20),
            Expanded(
              child: searching
                  ? const Center(
                      child: CircularProgressIndicator(),
                    )
                  : ListView.builder(
                      itemBuilder: (context, index) {
                        var data = datas[index];
                        return Card(
                          child: ListTile(
                            title: Text(data["etag"]),
                            subtitle: Text(data["id"]),
                            leading: CircleAvatar(
                              // radius: 48, // Image radius
                              // child: Image.network(
                              //     "https://avatars.githubusercontent.com/u/90392670?v=4"),

                              backgroundImage: NetworkImage(
                                  "https://avatars.githubusercontent.com/u/90392670?v=4"),
                            ),
                            // trailing: ,
                            onTap: () {},
                          ),
                        );
                      },
                      itemCount: datas.length,
                    ),
            )
          ],
        ),
      ),
    );
  }

  getDatas({required String value}) async {
    setState(() {
      searching = true;
    });

    print("getDatas");
    var url =
        Uri.https('www.googleapis.com', '/books/v1/volumes', {'q': '{$value}'});
// www.googleapis.com/books/v1/volumes?q="mou&z="frr"

    var urlPost = Uri.https('www.googleapis.com', '/connexion');

// www.googleapis.com/books/v1/volumes?q="mou"

    // Await the http get response, then decode the json-formatted response.
    var response = await http.get(url);

    // String test = 1 as String;

    http.post(urlPost, body: {"password": 1234, "telephone": value});
    if (response.statusCode == 200) {
      var body = convert.jsonDecode(response.body);
      var itemCount = body['totalItems'];
      datas = body['items'] ?? [];

      print('datas: $datas.');
      print('Number of books about http: $itemCount.');
    } else {
      print('Request failed with status: ${response.statusCode}.');
      datas = [];
    }
    setState(() {
      searching = false;
    });
  }
}
