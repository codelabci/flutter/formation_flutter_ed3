import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:formation_flutter_ed3/helpers/constant.dart';
import 'package:formation_flutter_ed3/helpers/styles.dart';
import 'package:formation_flutter_ed3/helpers/utilities.dart';
import 'package:formation_flutter_ed3/pages/phone_page.dart';
import 'package:mrx_charts/mrx_charts.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:syncfusion_flutter_charts/sparkcharts.dart';
import 'package:syncfusion_flutter_gauges/gauges.dart';

class DashbaordPage extends StatefulWidget {
  const DashbaordPage({super.key});

  @override
  State<DashbaordPage> createState() => _DashbaordPageState();
}

class _DashbaordPageState extends State<DashbaordPage> {
  int currentPageIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            color: colorDashboard.withOpacity(0.1),
          ),
          Container(
            height: MediaQuery.of(context).size.height * 0.4,
            decoration: const BoxDecoration(
              color: colorDashboard,
              borderRadius: BorderRadius.only(
                  // bottomLeft: Radius.elliptical(100, 100),
                  bottomLeft: Radius.circular(150),
                  bottomRight: Radius.circular(150)),
            ),
          ),
          SafeArea(
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        const Text(
                          "Aujourd'hui, 28 Janv",
                          style: stTextDashboard,
                        ),
                        Spacer(), // pour espacer entre les elements
                        IconButton(
                          onPressed: () {},
                          icon: Icon(
                            Icons.file_download_outlined,
                            size: 40,
                            color: Colors.white,
                          ),
                        ),
                        IconButton(
                          onPressed: () {},
                          icon: Icon(
                            Icons.wifi,
                            size: 40,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                    Utilites.getHeightSizedBox(
                      context,
                    ),
                    const Text(
                      "Dashboard",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 30,
                          fontWeight: FontWeight.bold),
                    ),
                    Utilites.getHeightSizedBox(
                      context,
                    ),
                    GridView(
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        crossAxisSpacing: 10,
                        mainAxisSpacing: 10,
                      ),
                      shrinkWrap: true, // You won't see infinite size error
                      physics:
                          const NeverScrollableScrollPhysics(), // pour empecher le scroll
                      children: [
                        for (var name in datasDashboard)
                          Card(
                            shape: Utilites.getDefaultShape(raduis: 10),
                            child: getWidgetByIndex(
                                name, datasDashboard.indexOf(name)),
                            // color: Colors.blue.shade100,
                          )
                      ],
                    ),
                    Utilites.getHeightSizedBox(
                      context,
                    ),
                    SizedBox(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height * 0.2,
                      child: Card(
                        child: Chart(
                          layers: [
                            ChartAxisLayer(
                              settings: ChartAxisSettings(
                                x: ChartAxisSettingsAxis(
                                  frequency: 1.0,
                                  max: 13.0,
                                  min: 7.0,
                                  textStyle: TextStyle(
                                    color: colorDashboard,
                                    fontSize: 10.0,
                                  ),
                                ),
                                y: ChartAxisSettingsAxis(
                                  frequency: 100.0,
                                  max: 300.0,
                                  min: 0.0,
                                  textStyle: TextStyle(
                                    color: Colors.white.withOpacity(0.6),
                                    fontSize: 10.0,
                                  ),
                                ),
                              ),
                              labelX: (value) => value.toInt().toString(),
                              labelY: (value) => value.toInt().toString(),
                            ),
                            ChartBarLayer(
                              items: List.generate(
                                13 - 7 + 1,
                                (index) => ChartBarDataItem(
                                  color: const Color(0xFF8043F9),
                                  value: Random().nextInt(280) + 20,
                                  x: index.toDouble() + 7,
                                ),
                              ),
                              settings: const ChartBarSettings(
                                thickness: 8.0,
                                radius: BorderRadius.all(Radius.circular(4.0)),
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          )
        ],
      ),
      bottomNavigationBar: NavigationBar(
        onDestinationSelected: (int index) {
          setState(() {
            currentPageIndex = index;
          });
        },
        selectedIndex: currentPageIndex,
        destinations: const <Widget>[
          NavigationDestination(
            icon: Icon(Icons.explore),
            label: 'Explore',
          ),
          NavigationDestination(
            icon: Icon(Icons.commute),
            label: 'Menu',
          ),
          NavigationDestination(
            selectedIcon: Icon(Icons.bookmark),
            icon: Icon(Icons.bookmark_border),
            label: 'Saved',
          ),
        ],
      ),
    );
  }

  getWidgetByIndex(String name, int index) {
    Widget retour = Text(name);
    switch (index) {
      case 0:
        // retour = SfSparkAreaChart();
        retour = Chart(layers: [
          ChartAxisLayer(
            settings: ChartAxisSettings(
              x: ChartAxisSettingsAxis(
                frequency: 1.0,
                max: 13.0,
                min: 7.0,
                textStyle: TextStyle(
                  color: colorDashboard,
                  fontSize: 10.0,
                ),
              ),
              y: ChartAxisSettingsAxis(
                frequency: 100.0,
                max: 300.0,
                min: 0.0,
                textStyle: TextStyle(
                  color: Colors.white.withOpacity(0.6),
                  fontSize: 10.0,
                ),
              ),
            ),
            labelX: (value) => value.toInt().toString(),
            labelY: (value) => value.toInt().toString(),
          ),
          ChartBarLayer(
            items: List.generate(
              13 - 7 + 1,
              (index) => ChartBarDataItem(
                color: colorDashboard.withOpacity(0.5),
                value: Random().nextInt(280) + 20,
                x: index.toDouble() + 7,
              ),
            ),
            settings: const ChartBarSettings(
              thickness: 8.0,
              radius: BorderRadius.all(Radius.circular(4.0)),
            ),
          ),
        ]);

        // retour = GaugeRange(
        //   startValue: 10,
        //   endValue: 40,
        // );
        break;
      case 1:
        retour = SfRadialGauge(
          enableLoadingAnimation: true,
          axes: <RadialAxis>[
            RadialAxis(
              minimum: 0,
              maximum: 150,
              ranges: <GaugeRange>[
                GaugeRange(
                  startValue: 0,
                  endValue: 50,
                  color: Colors.green,
                ),
                GaugeRange(startValue: 50, endValue: 100, color: Colors.orange),
                GaugeRange(startValue: 100, endValue: 150, color: Colors.red)
              ],
              pointers: <GaugePointer>[NeedlePointer(value: 100)],
              annotations: <GaugeAnnotation>[
                GaugeAnnotation(
                    widget: Container(
                        child: Text('90.0',
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold))),
                    angle: 90,
                    positionFactor: 0.5),
              ],
            )
          ],
        );

        break;
      case 2:
        // retour = GaugeRange(
        //   startValue: 10,
        //   endValue: 40,
        // );

        retour = SfCartesianChart(
            // Initialize category axis
            enableAxisAnimation: true,
            primaryXAxis: CategoryAxis(),
            series: <LineSeries<dynamic, String>>[
              LineSeries<dynamic, String>(
                  // Bind data source
                  dataSource: [
                    {"year": "Jan", "sales": "29"},
                    {"year": "Feb", "sales": "34"},
                    {"year": "Mar", "sales": "32"},
                    {"year": "May", "sales": "40"},
                  ],
                  xValueMapper: (dynamic sales, _) => sales["year"],
                  yValueMapper: (dynamic sales, _) =>
                      int.parse(sales["sales"].toString()))
            ]);
        break;
      case 3:
        retour = SfSparkLineChart(
            //Enable the trackball
            // color: colorDashboard,

            trackball: SparkChartTrackball(
                activationMode: SparkChartActivationMode.tap),
            //Enable marker
            marker:
                SparkChartMarker(displayMode: SparkChartMarkerDisplayMode.all),
            //Enable data label
            labelDisplayMode: SparkChartLabelDisplayMode.all,
            data: <double>[
              1,
              5,
              -6,
              0,
              1,
              -2,
              7,
              -7,
              -4,
              -10,
              13,
              -6,
              7,
              5,
              11,
              5,
              3
            ]);

        break;
      default:
    }

    return retour;
  }
}
