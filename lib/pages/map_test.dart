import 'package:flutter/material.dart';
import 'package:flutter_contacts/flutter_contacts.dart';
import 'package:geolocator/geolocator.dart';
import 'package:map_launcher/map_launcher.dart';

class MapTest extends StatefulWidget {
  MapTest({Key? key}) : super(key: key);

  @override
  State<MapTest> createState() => _MapTestState();
}

class _MapTestState extends State<MapTest> {
  testContact() async {
    print("testContact");
    if (await FlutterContacts.requestPermission()) {
      // Get all contacts (lightly fetched)
      List<Contact> contacts = await FlutterContacts.getContacts(
        withProperties: true,
      );
      for (var contact in contacts) {
        print("contact ::: $contact");
      }
    }
  }

  @override
  void initState() {
    super.initState();
    // testContact();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: ElevatedButton(
          // onPressed: () => launchMap(),
          onPressed: launchMap,
          child: Text("Map google"),
        ),
      ),
    );
  }

  launchMap() async {
    print("launchMap");
    Position? initialPosition = await _determinePosition();
    print("initialPosition ::: $initialPosition");
    if (initialPosition != null) {
      bool isGoogleMaps =
          await MapLauncher.isMapAvailable(MapType.google) ?? false;

      if (isGoogleMaps) {
        final availableMaps = await MapLauncher.installedMaps;

        for (int i = 0; i < availableMaps.length; i++) {
          if (availableMaps[i].mapType == MapType.google) {
            availableMaps[i].showDirections(
                // destination: Coords(9.460954, -5.639346),
                destination: Coords(5.460672182356164, -4.042424371164287),
                origin:
                    Coords(initialPosition.latitude, initialPosition.longitude),
                destinationTitle: "Cette formation est donnée par Codelab CI",
                originTitle: "Notre position");

            // availableMaps[i].showMarker(
            //   coords:
            //       Coords(initialPosition.latitude, initialPosition.longitude),
            //   title: "Codelab CI",
            //   description: "Cette formation est donnée par Codelab CI",
            // );
            break;
          }
        }
      }
    }
  }

  Future<Position?> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.
      //return Future.error('Location services are disabled.');
      return null;
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.
        //return Future.error('Location permissions are denied');
        return null;
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      //setState(() {});
      // return Future.error(
      //     'Location permissions are permanently denied, we cannot request permissions.');
      return null;
    }

    // When we reach here, permissions are granted and we can
    // continue accessing the position of the device.

    return await Geolocator.getCurrentPosition();
  }
}
