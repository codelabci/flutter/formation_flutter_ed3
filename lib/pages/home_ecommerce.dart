import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:formation_flutter_ed3/blocs/panier.dart';
import 'package:formation_flutter_ed3/helpers/constant.dart';
import 'package:formation_flutter_ed3/helpers/styles.dart';
import 'package:formation_flutter_ed3/helpers/utilities.dart';
import 'package:formation_flutter_ed3/models/product.dart';
import 'package:provider/provider.dart';

class HomeEcommerce extends StatefulWidget {
  HomeEcommerce({Key? key}) : super(key: key);

  @override
  State<HomeEcommerce> createState() => _HomeEcommerceState();
}

class _HomeEcommerceState extends State<HomeEcommerce> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // initDataBase();
  }

  // initDataBase() async {
  //   print('initDataBase');
  //   final conn = await MySqlConnection.connect(
  //     ConnectionSettings(
  //         host: '192.168.1.6',
  //         port: 8889,
  //         user: 'root',
  //         db: 'close',
  //         password: 'root'),
  //   );
  //   print('conn ::: $conn');

  //   var results = await conn.query(
  //     'select * from utilisateur_android',
  //   );
  //   for (var row in results) {
  //     print('Rows : $row');
  //   }
  //   // Finally, close the connection
  //   await conn.close();
  // }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: [
                const ListTile(
                  contentPadding: EdgeInsets.zero,
                  title: Text(
                    "Hey Nency,",
                    style: styleTitle,
                  ),
                  subtitle: Text("Begin your shopping !!"),
                  trailing: Icon(Icons.notification_important_outlined),
                ),
                Container(
                  // padding: EdgeInsets.all(8.0),
                  decoration: BoxDecoration(color: greenFondEcommerce),
                  height: Utilites.getHeightByMediaQuery(context, percent: 0.2),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Enjoy Upto \n50% Discount"),
                          ElevatedButton(
                            onPressed: () {},
                            child: Text("Subscribe"),
                          )
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Image.asset(
                          "images/1.jpeg",
                        ),
                      )
                    ],
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Top Categories',
                      style: styleBloc,
                    ),
                    TextButton(child: Text('SEE ALL'), onPressed: () {}),
                  ],
                ),
                SizedBox(
                  height:
                      Utilites.getHeightByMediaQuery(context, percent: 0.06),
                  child: ListView.separated(
                    separatorBuilder: (context, index) => SizedBox(
                      width: Utilites.getWidthByMediaQuery(context,
                          percent: 0.025),
                    ),
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index) {
                      return Container(
                        padding: EdgeInsets.all(10),
                        // margin: EdgeInsets.all(4),
                        decoration: BoxDecoration(
                            color: Colors.grey.shade300,
                            borderRadius: BorderRadius.circular(4)),
                        child: Icon(
                            index % 2 == 1 ? Icons.settings : Icons.dashboard),
                      );
                    },
                    itemCount: 10,
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'New Arrivals',
                      style: styleBloc,
                    ),
                    TextButton(child: Text('VIEW MORE'), onPressed: () {}),
                  ],
                ),
                ListView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemBuilder: (context, index) {
                    var currentProduct = datasProduct[index];
                    return OpenContainer(
                      // openElevation: 0.0,
                      // transitionDuration: const Duration(milliseconds: 500),
                      tappable: true,
                      transitionType: ContainerTransitionType.fadeThrough,

                      closedBuilder: (context, action) {
                        // action.call();

                        return ProductWidget(currentProduct: currentProduct);
                      },
                      openBuilder: (context, action) {
                        return Scaffold(
                          appBar: AppBar(
                            title: Text(currentProduct.name!),
                            actions: [
                              IconButton(
                                  onPressed: () {},
                                  icon: Icon(Icons.shopping_cart))
                            ],
                          ),
                          body: Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: Center(
                              child: Column(
                                children: [
                                  Image.asset(
                                    "images/1.jpeg",
                                  ),
                                  Row(
                                    children: [
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            currentProduct.name ?? "",
                                            style: styleTitleProduct,
                                          ),
                                          Text(
                                            currentProduct.price ?? "",
                                            style: styleBloc,
                                          ),
                                        ],
                                      ),
                                      Spacer(),
                                      ElevatedButton(
                                          onPressed: () {},
                                          child: Row(
                                            children: [
                                              Icon(Icons.star_border),
                                              Text(currentProduct.note ?? "")
                                            ],
                                          ))
                                    ],
                                  ),
                                  const Text(
                                    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
                                    textAlign: TextAlign.justify,
                                  )
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                    );
                  },
                  itemCount: datasProduct.length,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class ProductWidget extends StatelessWidget {
  const ProductWidget({
    Key? key,
    required this.currentProduct,
  }) : super(key: key);

  final Product currentProduct;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(4),
      decoration: BoxDecoration(
          color: Colors.grey.shade300, borderRadius: BorderRadius.circular(4)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          SizedBox(
              height: Utilites.getHeightByMediaQuery(context, percent: 0.2),
              width: Utilites.getWidthByMediaQuery(context, percent: 0.3),
              child: Image.asset(
                currentProduct.image ?? "",
                // fit: BoxFit.contain,
              )),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                currentProduct.name ?? "",
                style: styleTitleProduct,
              ),
              Text(
                currentProduct.price ?? "",
                style: styleBloc,
              ),
              ElevatedButton(
                  onPressed: () {},
                  child: Row(
                    children: [
                      Icon(Icons.star_border),
                      Text(currentProduct.note ?? "")
                    ],
                  ))
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              IconButton(
                onPressed: () {},
                icon: Icon(currentProduct.isFavorite
                    ? Icons.favorite
                    : Icons.favorite_border),
              ),
              IconButton(
                onPressed: () {
                  context.read<PanierChanger>().setPanier(
                        currentProduct.name!,
                      );
                  // ScaffoldMessenger.of(context).showSnackBar(
                  //   SnackBar(
                  //       content:
                  //           Text(context.watch<PanierChanger>().getPanier)),
                  // );
                },
                icon: Icon(Icons.add),
              ),
              if (currentProduct.name! ==
                  context.watch<PanierChanger>().getName)
                Stack(
                  alignment: Alignment.topRight,
                  children: [
                    Icon(
                      Icons.shopping_cart,
                      size: 50,
                    ),
                    Container(
                      padding: EdgeInsets.all(4),
                      decoration: BoxDecoration(
                        color: Colors.orangeAccent,
                        borderRadius: BorderRadius.circular(40),
                      ),
                      child: Text(context
                          .watch<PanierChanger>()
                          .getQuantite
                          .toString()),
                    )
                  ],
                ),
              // Text(context.watch<PanierChanger>().getPanier)
            ],
          )
        ],
      ),
    );
  }
}
