import 'package:flutter/material.dart';
import 'package:formation_flutter_ed3/blocs/langage.dart';
import 'package:formation_flutter_ed3/blocs/theme.dart';
import 'package:formation_flutter_ed3/helpers/constant.dart';
import 'package:formation_flutter_ed3/helpers/repository.dart';
import 'package:formation_flutter_ed3/helpers/utilities.dart';
import 'package:formation_flutter_ed3/l10n/L10n.dart';
import 'package:formation_flutter_ed3/models/user.dart';
import 'package:formation_flutter_ed3/pages/api_page.dart';
import 'package:formation_flutter_ed3/pages/contacts_test.dart';
import 'package:formation_flutter_ed3/pages/dashboard.dart';
import 'package:formation_flutter_ed3/pages/database_page.dart';
import 'package:formation_flutter_ed3/pages/edit_user.dart';
import 'package:formation_flutter_ed3/pages/home_ecommerce.dart';
import 'package:formation_flutter_ed3/pages/login.dart';
import 'package:formation_flutter_ed3/pages/login2.dart';
import 'package:formation_flutter_ed3/pages/map_test.dart';
import 'package:formation_flutter_ed3/pages/message_app.dart';
import 'package:formation_flutter_ed3/pages/micro_page.dart';
import 'package:formation_flutter_ed3/pages/phone_page.dart';
import 'package:formation_flutter_ed3/pages/scan_qrcode.dart';
import 'package:formation_flutter_ed3/widgets/user_widget.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class HomePage extends StatefulWidget {
  bool isUseInDatabase;
  HomePage({Key? key, this.isUseInDatabase = false}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<User> datasUserInLocalBD = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    initDatas();
  }

  initDatas() async {
    if (widget.isUseInDatabase) {
      var mapUsers = await SQLHelper.getUsers();

      print(mapUsers);

      if (mapUsers.isNotEmpty) {
        for (var i = 0; i < mapUsers.length; i++) {
          datasUserInLocalBD.add(User.fromMap(mapUsers[i]));
        }
        setState(
            () {}); // pour rafraichir le rendu car le build a ete executer avnt que le for prenne fin
      }
    }
  }

  bool isShowProfil = false;
  bool isChangeTheme = false;
  @override
  Widget build(BuildContext context) {
    print("object");
    return Scaffold(
      appBar: widget.isUseInDatabase
          ? null
          : AppBar(
              title: Text("Accueil"),
              actions: [
                PopupMenuButton(
                  itemBuilder: (context) {
                    return datasLanguage.map((String choice) {
                      return PopupMenuItem<String>(
                        value: choice,
                        child: Text(choice),
                        onTap: () {
                          Locale locale = const Locale("fr");

                          switch (choice) {
                            case arabe:
                              locale = const Locale("ar");

                              break;
                            case anglais:
                              locale = const Locale("en");

                              break;
                            case francais:
                              locale = const Locale("fr");

                              break;
                            case espagnol:
                              locale = const Locale("es");

                              break;
                            default:
                          }
                          context.read<LangageChanger>().setLocale(
                              locale); // emet l'evenement de changement
                        },
                      );
                    }).toList();
                  },
                  icon: const Icon(Icons.language),
                )
              ],
            ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: SafeArea(
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Flexible(
                    child: Text(
                      'Liste des utilisateurs',
                      style: TextStyle(fontSize: 20),
                    ),
                  ),
                  if (!widget.isUseInDatabase)
                    ElevatedButton(
                      onPressed: () {
                        // test();

                        // traitement pour la navigation vers le editUserPage

                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => EditUserPage(
                                  // user: datasUser[1],
                                  )),
                        );
                      },
                      child: Text('Ajouter'),
                    ),
                ],
              ),

              // emetteur d'evenement
              SwitchListTile(
                value: isChangeTheme,

                onChanged: (value) {
                  print(value);
                  context
                      .read<ThemeChanger>()
                      .setTheme(value ? ThemeData.dark() : ThemeData.light());
                  setState(() {
                    isChangeTheme = value;
                  });
                },
                // isThreeLine: true,
                title: const Text("Changer le theme"),
                subtitle: const Text("Mise en oeuvre de Provider"),
                secondary: Icon(Icons.settings),
              ),

              ListTile(
                title: Text("Internationalisation"),
                subtitle: Text(
                  "content : ${AppLocalizations.of(context)!.helloWorld}",
                ),
              ),
              Expanded(
                // premiere facon de faire un listView
                // child: ListView(
                //   children: [
                //     UserWidget(
                //       user: datasUser[0],
                //     ),
                //     UserWidget(
                //       user: datasUser[1],
                //     ),
                //     UserWidget(
                //       user: datasUser[2],
                //     ),
                //   ],
                // ),
                // deuxieme facon de faire un listView
                child: ListView.builder(
                  itemBuilder: (context, index) {
                    return UserWidget(
                        isUseInDatabase: widget.isUseInDatabase,
                        user: widget.isUseInDatabase
                            ? datasUserInLocalBD[index]
                            : datasUser[index]);
                  },
                  itemCount: widget.isUseInDatabase
                      ? datasUserInLocalBD.length
                      : datasUser.length,
                ),
              )
            ],
          ),
        ),
      ),
      drawer: Drawer(
        // backgroundColor: Colors.red,
        // elevation: 0,

        // child: ElevatedButton(onPressed: () {}, child: Text("Kone")),
        child: ListView(
          children: [
            // DrawerHeader(
            //   child: Column(
            //     children: [
            //       CircleAvatar(),
            //       Text("Coulibaly Aicha"),
            //     ],
            //   ),
            //   decoration: BoxDecoration(color: Colors.blue),
            // ),
            UserAccountsDrawerHeader(
              onDetailsPressed: () {
                setState(() {
                  isShowProfil = !isShowProfil;
                });
              },
              currentAccountPictureSize: const Size.square(100.0),
              currentAccountPicture: Center(child: Image.asset(logo)),
              accountName: const Text("Codelab CI"),
              accountEmail: const Text(
                "Contact : +225 2721330224",
              ),
            ),
            if (isShowProfil)
              Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      IconButton(
                        onPressed: () {
                          // Utilities.launch(Uri(
                          //   scheme: 'tel',
                          //   path: "+$numberLMS",
                          // ));
                        },
                        icon: const Icon(
                          Icons.call,
                          color: Colors.blue,
                          size: 30,
                        ),
                      ),
                      IconButton(
                        onPressed: () {
                          // Utilities.launch(
                          //   Uri(
                          //     scheme: 'https',
                          //     host: 'wa.me',
                          //     path: '/$numberLMS',
                          //   ),
                          // );
                        },
                        icon: const Icon(
                          Icons.whatsapp,
                          color: Color(0xFF25D366),
                          size: 30,
                        ),
                      ),
                      IconButton(
                        onPressed: () {
                          // Utilities.launch(Uri(
                          //   scheme: 'tel',
                          //   path: "+$numberLMS",
                          // ));
                        },
                        icon: const Icon(
                          Icons.language,
                          color: Colors.blue,
                          size: 30,
                        ),
                      ),
                    ],
                  ),
                  const Divider()
                ],
              ),
            ListTile(
              title: Text(AppLocalizations.of(context)!.dashboard),
              onTap: () {
                // Utilites.toast(context: context, message: "Message App");
                // ordre important
                Navigator.pop(context); // la pile des ecrans
                Utilites.navigatorPush(context: context, view: DashbaordPage());
              },
              leading: const Icon(Icons.dashboard),
            ),
            ListTile(
              title: const Text('E-commerce'),
              onTap: () {
                // Utilites.toast(context: context, message: "Message App");
                // ordre important
                Navigator.pop(context); // la pile des ecrans
                Utilites.navigatorPush(context: context, view: HomeEcommerce());
              },
              leading: const Icon(Icons.shop),
            ),
            ListTile(
              title: const Text('Scan QrCode'),
              onTap: () {
                // Utilites.toast(context: context, message: "Message App");
                // ordre important
                Navigator.pop(context); // la pile des ecrans
                Utilites.navigatorPush(
                    context: context, view: ScanQrcodePage());
              },
              leading: const Icon(Icons.qr_code),
            ),
            ListTile(
              title: const Text('Micro/Record'),
              onTap: () {
                // Utilites.toast(context: context, message: "Message App");
                // ordre important
                Navigator.pop(context); // la pile des ecrans
                Utilites.navigatorPush(context: context, view: MicroPage());
              },
              leading: const Icon(Icons.mic_rounded),
            ),
            ListTile(
              title: const Text('CRUD with database'),
              onTap: () {
                // Utilites.toast(context: context, message: "Message App");
                // ordre important
                Navigator.pop(context); // la pile des ecrans
                Utilites.navigatorPush(context: context, view: HomeEcommerce());
              },
              leading: const Icon(Icons.phone),
            ),
            ListTile(
              title: const Text('Phone app'),
              onTap: () {
                // Utilites.toast(context: context, message: "Message App");
                // ordre important
                Navigator.pop(context); // la pile des ecrans
                Utilites.navigatorPush(context: context, view: PhoneAppPage());
              },
              leading: const Icon(Icons.phone),
            ),
            ListTile(
              title: const Text('Appel api'),
              onTap: () {
                // Utilites.toast(context: context, message: "Message App");
                // ordre important
                Navigator.pop(context); // la pile des ecrans
                Utilites.navigatorPush(context: context, view: ApiPage());
              },
              leading: const Icon(Icons.phone),
            ),
            ListTile(
              title: const Text('Message apps'),
              onTap: () {
                // Utilites.toast(context: context, message: "Message App");
                // ordre important
                Navigator.pop(context); // la pile des ecrans
                Utilites.navigatorPush(context: context, view: MessageApp());
              },
              leading: const Icon(Icons.message),
            ),
            ListTile(
              title: const Text('Connexion page'),
              onTap: () {
                // Utilites.toast(context: context, message: "Message App");
                // ordre important
                Navigator.pop(context); // la pile des ecrans
                Utilites.navigatorPush(context: context, view: LoginPage());
              },
              leading: const Icon(Icons.login),
            ),
            ListTile(
              title: const Text('Connexion page with template'),
              onTap: () {
                // Utilites.toast(context: context, message: "Message App");
                // ordre important
                Navigator.pop(context); // la pile des ecrans
                Utilites.navigatorPush(context: context, view: LoginScreen());
              },
              leading: const Icon(Icons.login),
            ),
            ListTile(
              title: const Text('Acceder aux contacts du phone'),
              onTap: () {
                // Utilites.toast(context: context, message: "Message App");
                // ordre important
                Navigator.pop(context); // la pile des ecrans
                test();
              },
              leading: const Icon(Icons.login),
            ),
            ListTile(
              title: const Text('Maps'),
              onTap: () {
                // Utilites.toast(context: context, message: "Message App");
                // ordre important
                Navigator.pop(context); // la pile des ecrans
                Utilites.navigatorPush(context: context, view: MapTest());
              },
              leading: const Icon(Icons.map),
            ),
          ],
        ),
      ),
    );
  }
}
