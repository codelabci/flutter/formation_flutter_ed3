import 'package:flutter/material.dart';
import 'package:formation_flutter_ed3/helpers/constant.dart';
import 'package:formation_flutter_ed3/helpers/repository.dart';
import 'package:formation_flutter_ed3/models/user.dart';
import 'package:formation_flutter_ed3/pages/home.dart';

class EditUserPage extends StatefulWidget {
  User? user;
  bool isUseInDatabase;

  EditUserPage({Key? key, this.user, this.isUseInDatabase = false})
      : super(key: key);
  // EditUserPage({super.key});

  @override
  State<EditUserPage> createState() => _EditUserPageState();
}

class _EditUserPageState extends State<EditUserPage> {
  final formKey = GlobalKey<FormState>();

  final TextEditingController _editController = TextEditingController();

  // User data = User(nom: "");

  bool isNewUser = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    // if (widget.user == null) {
    //   widget.user = User(nom: "");
    // }

    print("initState");
    print("before :: $isNewUser");
    print("before widget.user :: ${widget.user}");

    isNewUser = widget.user == null;
    print("after :: $isNewUser");
    widget.user ??= User(nom: "");
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    widget.user = null;
  }

  @override
  Widget build(BuildContext context) {
    print("cvbn");
    return Scaffold(
        appBar: widget.isUseInDatabase
            ? null
            : AppBar(
                title: Text("Editer user"),
              ),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: SafeArea(
            child: Form(
              key: formKey,
              // onChanged: validator,

              child: ListView(
                children: [
                  TextFormField(
                    // initialValue: widget.user!.nom,
                    initialValue: widget.user?.nom,
                    // initialValue: widget.user != null ? widget.user!.nom : null,
                    decoration: InputDecoration(label: Text("Nom")),
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return "Champ requis";
                      }
                      return null;
                    },
                    onSaved: (value) {
                      // if (widget.user != null) {
                      //   // widget.user!.nom = value != null ? value : "";
                      //   widget.user!.nom = value ?? "";
                      // }
                      widget.user?.nom = value ?? "";
                    },
                    onChanged: (value) {
                      print("onChanged :: $value");
                    },
                    // keyboardType: TextInputType.phone,
                  ),
                  TextFormField(
                    initialValue: widget.user?.prenoms,
                    decoration: InputDecoration(label: Text("Prenoms")),
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    onSaved: (value) {
                      // data.prenoms = value;
                      widget.user?.prenoms = value;
                    },
                    onChanged: (value) {
                      print("onChanged :: $value");
                    },
                  ),
                  TextFormField(
                    // initialValue: widget.user?.email,
                    controller: _editController,
                    decoration: InputDecoration(label: Text("Email")),
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    validator: (value) {
                      if (value != null && !value.contains("@")) {
                        return "Ce champ doit etre un email";
                      }
                      return null;
                    },
                    onSaved: (value) {
                      // data.email = value;
                      widget.user?.email = value;
                    },
                    onChanged: (value) {
                      print("onChanged :: $value");
                    },
                  ),
                  // Checkbox(value: true, onChanged: (value) {}),
                  ElevatedButton(
                      onPressed: submit,
                      child: Text(isNewUser ? "Ajouter" : "Modifier")),
                ],
              ),
            ),
          ),
        ));
  }

  validator() {
    print("validator form ::: begin");
    String email = _editController.text;
    if (email == null || email.isEmpty) {
      if (email != null && !email.contains("@")) {
        return "Ce champ doit etre un email";
      }
    }
  }

  submit() {
    // logique submit

    var formState = formKey.currentState;

    if (formState!.validate()) {
      // debut de la logique de traitement de donnee

      formState.save(); // execute toutes les methodes onSaved de ses inputs

      if (widget.isUseInDatabase) {
        isNewUser
            ? SQLHelper.createUser(widget.user!)
            : SQLHelper.updateUser(widget.user!);
      } else {
        if (isNewUser) {
          datasUser.add(widget.user!); //  ajout de nouvel utilisateur
        }
      }

      // navigation
      Navigator.of(context).pushAndRemoveUntil(
          // supprimer toutes les vues
          MaterialPageRoute(builder: (BuildContext context) => HomePage()),
          (Route<dynamic> route) => false);
    }
  }
}
