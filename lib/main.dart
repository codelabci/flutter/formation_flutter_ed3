import 'package:flutter/material.dart';
import 'package:formation_flutter_ed3/blocs/langage.dart';
import 'package:formation_flutter_ed3/blocs/panier.dart';
import 'package:formation_flutter_ed3/blocs/theme.dart';
import 'package:formation_flutter_ed3/l10n/L10n.dart';
import 'package:formation_flutter_ed3/pages/contacts_test.dart';
import 'package:formation_flutter_ed3/pages/dashboard.dart';
import 'package:formation_flutter_ed3/pages/home.dart';
import 'package:provider/provider.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() {
  runApp(const MyAppWithThmeChanger());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,

      title: 'Flutter Test',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        useMaterial3: false,
      ),
      home: HomePage(),
      // home: DashbaordPage(),
    );
  }
}

class MyAppWithThmeChanger extends StatelessWidget {
  const MyAppWithThmeChanger({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    // declaration des providers
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<ThemeChanger>(
            create: (context) => ThemeChanger(ThemeData.light())),
        ChangeNotifierProvider<LangageChanger>(
            create: (context) => LangageChanger(L10n.allLocale.first)),
        ChangeNotifierProvider<PanierChanger>(
            create: (context) => PanierChanger("Premier produit", 2)),
      ],
      child: MaterialAppWithThemeChanger(),
    );
  }
}

class MaterialAppWithThemeChanger extends StatelessWidget {
  const MaterialAppWithThemeChanger({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print("building MaterialApp");
    final themeChanger = Provider.of<ThemeChanger>(context);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Test',
      // theme: ThemeData(
      //   primarySwatch: Colors.blue,
      //   useMaterial3: false,
      // ),

      localizationsDelegates: L10n.localizationsDelegates,
      supportedLocales: L10n.allLocale,

      locale:
          context.watch<LangageChanger>().getLocale(), //  ecouteur d'evenement

      // supportedLocales: L1

      theme: context.watch<ThemeChanger>().getTheme(), //  ecouteur d'evenement
      // theme: themeChanger.getTheme(), //  deuxieme facon d'ecouter l'evenement
      home: HomePage(),
      // home: DashbaordPage(),
    );
  }
}
