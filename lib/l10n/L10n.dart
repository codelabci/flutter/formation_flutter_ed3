import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class L10n {
  static const allLocale = [
    Locale('en'), // English
    Locale('es'), // Spanish
    Locale('fr'), // French
    Locale('ar'), // Arabe
  ];

  static const localizationsDelegates = [
    AppLocalizations.delegate, // Add this line

    GlobalMaterialLocalizations.delegate,
    GlobalWidgetsLocalizations
        .delegate, // direction des widgets selon la langue
    GlobalCupertinoLocalizations.delegate,
  ];
}
