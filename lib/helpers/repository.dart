import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:formation_flutter_ed3/helpers/utilities.dart';
import 'package:formation_flutter_ed3/models/user.dart';
import 'package:sqflite/sqflite.dart' as sql;

// id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,

class SQLHelper {
  static Future<sql.Database>? database;

  static Future<void> createTables(sql.Database database) async {
    await database.execute("""CREATE TABLE user(
        id integer primary key autoincrement,
        nom TEXT,
        prenoms TEXT,
        email TEXT
      )
      """);
  }
// id: the id of a user
// created_at: the time that the user was created. It will be automatically handled by SQLite

  static Future<sql.Database> db() async {
    database ??= sql.openDatabase(
      'codelabci.db',
      version: 1,
      onCreate: (sql.Database database, int version) async {
        await createTables(database);
        print("l");
      },
    );

    return database!;
  }

  // Create new item (journal)
  static Future<int> createUser(User data) async {
    print("createUser");
    // Utilites.begin("createUser");
    final db = await SQLHelper.db();

    final id = await db.insert('user', data.toMap(),
        conflictAlgorithm: sql.ConflictAlgorithm.replace);
    print(id);
    return id;
  }

  // Read all user (journals)
  static Future<List<Map<String, dynamic>>> getUsers() async {
    print("getUsers");

    final db = await SQLHelper.db();
    return db.query('user', orderBy: "id");
  }

  // Read a single item by id
  // The app doesn't use this method but I put here in case you want to see it
  static Future<List<Map<String, dynamic>>> getUser(int id) async {
    print("getUser");

    final db = await SQLHelper.db();
    return db.query('user', where: "id = ?", whereArgs: [id], limit: 1);
  }

  // Update an item by id
  static Future<int> updateUser(User data) async {
    print("updateUser");
    final db = await SQLHelper.db();

    final result = await db
        .update('user', data.toMap(), where: "id = ?", whereArgs: [data.id]);
    print(result);
    return result;
  }

  // Delete
  static Future<void> deleteUser(int id) async {
    print("deleteUser");
    final db = await SQLHelper.db();
    try {
      await db.delete("user", where: "id = ?", whereArgs: [id]);
    } catch (err) {
      debugPrint("Something went wrong when deleting an user: $err");
    }
  }
}
