import 'package:flutter/material.dart';
import 'package:formation_flutter_ed3/helpers/constant.dart';

const stAppName =
    TextStyle(color: Colors.white, fontSize: 30, fontWeight: FontWeight.w300);

const stTextDashboard =
    TextStyle(color: Colors.white54, fontSize: 20, fontWeight: FontWeight.w300);

const styleTitle = TextStyle(fontWeight: FontWeight.w600, fontSize: 20);
const styleBloc = TextStyle(fontWeight: FontWeight.w500, fontSize: 16);
const styleTitleProduct = TextStyle(fontWeight: FontWeight.w300);
const styleSeeAll =
    TextStyle(fontWeight: FontWeight.w500, color: appColorGreen1);
