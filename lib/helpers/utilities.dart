import 'package:flutter/material.dart';
import 'package:formation_flutter_ed3/pages/home.dart';

class Utilites {
  static navigatorPush({required BuildContext context, dynamic view}) {
    return Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => view ?? HomePage()),
    );
  }

  static navigatorPushAndRemoveUntil(
      {required BuildContext context, dynamic view}) {
    return Navigator.of(context).pushAndRemoveUntil(
        // supprimer toutes les vues

        MaterialPageRoute(
            builder: (BuildContext context) => view ?? HomePage()),
        (Route<dynamic> route) => false);
  }

  static getDefaultShape(
          {double raduis = 10.0, BorderSide side = BorderSide.none}) =>
      RoundedRectangleBorder(
        side: side,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(raduis),
          topRight: Radius.circular(raduis),
          bottomLeft: Radius.circular(raduis),
          bottomRight: Radius.circular(raduis),
        ),
      );

  static getShapeElevatedButton({double raduis = 18.0}) {
    return MaterialStateProperty.all<RoundedRectangleBorder>(
        RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(raduis),
      // side: BorderSide(color: Colors.red)
    ));
  }

  static getHeightSizedBox(BuildContext context, {double height = 0.025}) {
    return SizedBox(
      height: MediaQuery.of(context).size.height * height,
    );
  }

  static getWidthSizedBox(BuildContext context, {double width = 0.80}) {
    return SizedBox(
      height: MediaQuery.of(context).size.width * width,
    );
  }

  static getWidthByMediaQuery(BuildContext context, {double percent = 1}) {
    return MediaQuery.of(context).size.width * percent;
  }

  static getHeightByMediaQuery(BuildContext context, {double percent = 1}) {
    return MediaQuery.of(context).size.height * percent;
  }
}
