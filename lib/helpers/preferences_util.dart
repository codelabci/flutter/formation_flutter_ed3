import 'package:shared_preferences/shared_preferences.dart';

class PreferencesUtil {
  static SharedPreferences? _preferences;

  // Make this a singleton class.
  PreferencesUtil._privateConstructor();
  static final PreferencesUtil instance = PreferencesUtil._privateConstructor();

  static Future<SharedPreferences> getInstanceSharedPreferences() async {
    if (_preferences != null) {
      return _preferences!;
    }
    _preferences = await SharedPreferences.getInstance();
    return _preferences!;
  }

  PreferencesUtil._();
  Future _init() async {
    _preferences = await SharedPreferences.getInstance();
  }

  // remove key
  Future<bool> remove(String key) async {
    _preferences ??= await getInstanceSharedPreferences();
    return _preferences!.remove(key);
  }

// get string
  Future<String> getString(String key, {String defValue = ''}) async {
    if (_preferences == null)
      _preferences = await getInstanceSharedPreferences();

    return _preferences!.getString(key) ?? defValue;
  }

  // put string
  Future<bool> putString(String key, String value) async {
    if (_preferences == null)
      _preferences = await getInstanceSharedPreferences();
    return _preferences!.setString(key, value);
  }

  // get int
  Future<int> getInt(String key, {int defValue = 0}) async {
    if (_preferences == null)
      _preferences = await getInstanceSharedPreferences();
    return _preferences!.getInt(key) ?? defValue;
  }

  // put int
  Future<bool> putInt(String key, int value) async {
    if (_preferences == null)
      _preferences = await getInstanceSharedPreferences();
    return _preferences!.setInt(key, value);
  }

  // get int
  Future<bool> getBool(String key, {bool defValue = false}) async {
    if (_preferences == null)
      _preferences = await getInstanceSharedPreferences();
    return _preferences!.getBool(key) ?? defValue;
  }

  // put int
  Future<bool> putBool(String key, bool value) async {
    if (_preferences == null)
      _preferences = await getInstanceSharedPreferences();
    return _preferences!.setBool(key, value);
  }

  // get string
  static String getStringOld(String key, {String defValue = ''}) {
    if (_preferences == null) return defValue;
    return _preferences!.getString(key) ?? defValue;
  }

  // clear datas
  static Future<bool> clearDatas() async {
    if (_preferences == null)
      _preferences = await getInstanceSharedPreferences();

    return _preferences!.clear();
  }
}
