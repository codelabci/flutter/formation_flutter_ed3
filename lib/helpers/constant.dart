import 'package:flutter/material.dart';
import 'package:formation_flutter_ed3/models/product.dart';
import 'package:formation_flutter_ed3/models/user.dart';

// final List<User> datasUser = [ ou
final datasUser = [
  User(nom: "Coulibaly"),
  User(nom: "Traoré"),
  User(
    nom: "Ouedraogo",
  ),
];

const structureName = "Codelab CI";

const Color red = Color(0xFFFF0000);
const Color black = Color(0xFF000000);
const Color yellow = Color.fromRGBO(255, 235, 59, 1);

const appColorGreen1 = Color(0xFF075E54);
const appColorGreen2 = Color(0xFF128C7E);
const appColorGreen3 = Color(0XFF25D366);
const appColorGreen4 = Color(0XFFDCF8C6);
const appColorBlue = Color(0XFF34B7F1);
const appColorWhite = Color(0XFFECE5DD);
const colorWhite = Colors.white;

final greenFondEcommerce = appColorGreen3.withOpacity(0.3);
const greenButtonEcommerce = appColorGreen2;
final greenTextEcommerce = appColorGreen2.withOpacity(0.6);

const colorDashboard = Color.fromRGBO(33, 40, 48, 1);
const datasPopupMenuButton = [
  "Anti-Spams activé",
  "Gérer vos favoris",
  "Paramètres",
  "Actualités Orange Téléphone",
  "Aide",
];

final datasProduct = [
  Product(
      name: "Pink & Blue Top",
      price: "\$150.0",
      note: "4.5",
      isFavorite: true,
      image: "images/1.jpeg"),
  Product(
      name: "White off shoulder",
      price: "\$135.5",
      note: "3.5",
      image: "images/2.jpeg"),
  Product(
      name: "Red & Blue Jacket",
      price: "\$170.0",
      note: "4.0",
      isFavorite: true,
      image: "images/3.jpeg"),
  Product(
      name: "Trendy White Top",
      price: "\$129.9",
      note: "3.0",
      isFavorite: true,
      image: "images/4.jpeg"),
];

const defaultLastContent =
    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown";

const datasAllFileMessage = [
  {
    'name': '+454',
    "lastContent": defaultLastContent,
    "heure": "08:30",
    'isShow': true,
    'color': red,
  },
  {
    'name': 'Orange et Moi',
    "lastContent": defaultLastContent,
    "heure": "08:30",
    'isShow': true,
    'color': yellow,
  },
  {
    'name': 'MTN BonPlan',
    "lastContent": defaultLastContent,
    "heure": "08:30",
    'isShow': false,
    'color': Colors.blue,
  },
  {
    'name': '404',
    "lastContent": defaultLastContent,
    "heure": "08:30",
    'isShow': false,
    'color': Colors.orange,
  },
  {
    'name': 'Touré zié',
    "lastContent": defaultLastContent,
    "heure": "08:30",
    'isShow': true,
    'color': Colors.indigo,
  },
  {
    'name': 'Coulibaly zié',
    "lastContent": defaultLastContent,
    "heure": "08:30",
    'isShow': false,
    'color': Colors.red,
  },
  {
    'name': 'Diallo zié',
    "lastContent": defaultLastContent,
    "heure": "08:30",
    'isShow': false,
    'color': Colors.green,
  },
  {
    'name': 'Tall zié',
    "lastContent": defaultLastContent,
    "heure": "08:30",
    'isShow': false,
    'color': Colors.black,
  },
  {
    'name': 'Seriba',
    "lastContent": defaultLastContent,
    "heure": "08:30",
    'isShow': true,
    'color': Colors.green,
  },
  {
    'name': 'Moov Africa',
    "lastContent": defaultLastContent,
    "heure": "08:30",
    'isShow': false,
    'color': Colors.yellow,
  },
  {
    'name': 'Mardi Bonus',
    "lastContent": defaultLastContent,
    "heure": "08:30",
    'isShow': true,
    'color': Colors.blueGrey,
  },
  {
    'name': '0595054040',
    "lastContent": defaultLastContent,
    "heure": "08:30",
    'isShow': false,
    'color': Colors.grey,
  },
];

const lbArchives = "Archivées";
const lbSpam = "Spam et coversations bloques";
const lbToutMarque = "Tout marquer comme lu";
const lbParametres = "Paramètres";
const lbAide = "Aide et commentaires";
const datasPopupMenuButtonMessage = [
  lbToutMarque,
  lbArchives,
  lbSpam,
  lbParametres,
  lbAide,
];

const arabe = "arabe";
const francais = "français";
const anglais = "anglais";
const espagnol = "espagnol";
const datasLanguage = [
  arabe,
  francais,
  anglais,
  espagnol,
];

const logo = "images/logo.jpg";

final datasContentFileMessage = [
  {
    "content": defaultLastContent,
    "isMe": false,
    "jour": "Hier",
  },
  {
    "content": defaultLastContent,
    "isMe": false,
    "jour": "Hier",
  },
  {
    "content": defaultLastContent,
    "isMe": false,
    "jour": "Hier",
  },
  {
    "content": defaultLastContent,
    "isMe": true,
    "jour": "Hier",
  },
  {
    "content": defaultLastContent,
    "isMe": false,
    "jour": "Hier",
  },
  {
    "content": defaultLastContent,
    "isMe": false,
    "jour": "Aujourd'hui",
  },
  {
    "content": defaultLastContent,
    "isMe": true,
    "jour": "Aujourd'hui",
  },
  {
    "content": defaultLastContent,
    "isMe": true,
    "jour": "Aujourd'hui",
  },
];

const valRaduis = 10.0;

const raduis = Radius.circular(valRaduis);

const datasCurrentCall = [
  {
    'name': 'Touré zié',
    "phone": "0595054040",
    "heure": "08:30",
    'type': "Principal",
    'sim': 1,
    'group': "Aujourd'hui"
  },
  {
    'name': 'Koné irié',
    "phone": "0585054040",
    "heure": "08:20",
    'type': "Portable",
    'sim': 2,
    'group': "Aujourd'hui"
  },
  {
    'name': 'Kouassi seriba',
    "phone": "0575054040",
    "heure": "08:02",
    'type': "Principal",
    'sim': 1,
    'group': "Aujourd'hui"
  },
  {
    'name': 'Kouame',
    "phone": "0565054040",
    "heure": "08:01",
    'type': "Principal",
    'sim': 1,
    'group': "Aujourd'hui"
  },
  {
    'name': 'Mathias',
    "phone": "0555054040",
    "heure": "08:00",
    'type': "Portable",
    'sim': 2,
    'group': "Hier"
  },
  {
    'name': 'Assetou',
    "phone": "0545054040",
    "heure": "07:02",
    'type': "Portable",
    'sim': 1,
    'group': "Hier"
  },
  {
    'name': 'Aminata',
    "phone": "0535054040",
    "heure": "08:02",
    'type': "Portable",
    'sim': 2,
    'group': "Avant-hier"
  },
  {
    'name': 'Yelle',
    "phone": "0525054040",
    "heure": "08:00",
    'type': "Portable",
    'sim': 1,
    'group': "Avant-hier"
  },
  {
    'name': 'Kiki',
    "phone": "0515054040",
    "heure": "07:02",
    'type': "Portable",
    'sim': 2,
    'group': "Avant-hier"
  },
];

const valSizeIcon = 15.0;
const valWidthSizedbox = 5.0;

// const valRaduis = 10.0;
const valBorderRaduis = 30.0;

const datasContact = [
  "Adama Moov",
  "Maman",
  "Kone seriba",
  "Aboubacar sidik",
  "Macon konate",
];

const datasDashboard = [
  "Ventes",
  "Achats",
  "Stocks",
  "Commandes",
];
final datasDrawer = [];
